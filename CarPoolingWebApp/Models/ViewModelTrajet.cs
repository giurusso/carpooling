﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarPooling;

namespace CarPoolingWebApp.Models
{
    public class ViewModelTrajet
    {

        private List<Trajet> listeTrajet;


        public ViewModelTrajet()
        {
            ListeTrajet = new List<Trajet>();
            // GestionSource SourceManagement = new GestionSource();
            // IDataSource MaDataSource = SourceManagement.SelectDataSource();
            IDataSource MaDataSource = new DataSourceDemo();
            ListeTrajet = MaDataSource.Trajet();

        }

        public List<Trajet> ListeTrajet
        {
            get
            {
                return listeTrajet;
            }

            set
            {
                listeTrajet = value;
            }
        }

    }
}