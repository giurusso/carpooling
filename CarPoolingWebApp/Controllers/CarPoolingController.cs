﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarPooling;
using CarPoolingWebApp.Models;

namespace CarPoolingWebApp.Controllers
{
    public class CarPoolingController : Controller
    {
        // GET: CarPooling
        public ActionResult Index(Trajet monTrajet)
        {
            return View(new ViewModelTrajet());
        }

        public ActionResult About()
        {
            //ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Connexion()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult UserProfile()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ModifyProfile()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}