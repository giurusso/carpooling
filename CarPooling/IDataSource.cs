﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarPooling;

namespace CarPooling
{
   public interface IDataSource
    {
        List<Trajet> Trajet();
        List<User> getUsers();
        User getUser(string pAccount);
    }
}
