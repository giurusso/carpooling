﻿


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    public class Trajet
    {
        private string villeDeDepart;
        private string villeArrivee;
        private string heureDepart;
        private string nbParticipant;

        public string VilleDeDepart
        {
            get
            {
                return villeDeDepart;
            }

            set
            {
                villeDeDepart = value;
            }
        }

        public string VilleArrivee
        {
            get
            {
                return villeArrivee;
            }

            set
            {
                villeArrivee = value;
            }
        }

        public string HeureDepart
        {
            get
            {
                return heureDepart;
            }

            set
            {
                heureDepart = value;
            }
        }

        public string NbParticipant
        {
            get
            {
                return nbParticipant;
            }

            set
            {
                nbParticipant = value;
            }
        }
    }
}
