﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    public class DataSourceDemo:DataSource
    {
        public override List<Trajet> Trajet()
        {
            List<Trajet> maListe = new List<Trajet>();

            Trajet monTrajet = new Trajet();
            monTrajet.VilleDeDepart = "Bulle";
            monTrajet.VilleArrivee = "Lausanne";
            monTrajet.HeureDepart = "19h";
            monTrajet.NbParticipant = "2";
            maListe.Add(monTrajet);
            monTrajet = null;

            monTrajet = new Trajet();
            monTrajet.VilleDeDepart = "Lausanne";
            monTrajet.VilleArrivee = "Yverdon";
            monTrajet.HeureDepart = "17h";
            monTrajet.NbParticipant = "1";
            maListe.Add(monTrajet);
            monTrajet = null;

            monTrajet = new Trajet();
            monTrajet.VilleDeDepart = "Yverdon";
            monTrajet.VilleArrivee = "Avenches";
            monTrajet.HeureDepart = "12h";
            monTrajet.NbParticipant = "3";
            maListe.Add(monTrajet);
            monTrajet = null;

            monTrajet = new Trajet();
            monTrajet.VilleDeDepart = "Sion";
            monTrajet.VilleArrivee = "Genève";
            monTrajet.HeureDepart = "14h";
            monTrajet.NbParticipant = "5";
            maListe.Add(monTrajet);
            monTrajet = null;


            return maListe;

        } 

        public override List<User> getUsers()
        {
            List<User> mesUsers = new List<User>();
            User user1 = new User("toto");
            user1.Name = "toto";
            user1.FirstName = "tata";
            user1.Address = "Rue d'Yverdon 2";
            user1.City = "Yverdon";
            user1.PostalNumber = 1400;
            user1.DateBirth = new DateTime(1980, 02, 29);
            user1.PhoneNumber = "024 111 11 11";
            user1.DateInscription = new DateTime(2008, 12, 25);
            user1.Password = "1234";

            Profil user1Profil = new Profil();
            user1Profil.Brand = "Toyota";
            user1Profil.Model = "Avenis";
            user1Profil.PlateNumber = "VD 111 11";
            user1Profil.Year = 2015;
            user1Profil.Smoker = true;

            user1.Profils.Add(user1Profil);

            mesUsers.Add(user1);

            User user2 = new User("titi");
            user2.Name = "titi";
            user2.FirstName = "tutu";
            user2.Address = "Rue de Lausanne 20";
            user2.City = "Lausanne";
            user2.PostalNumber = 1004;
            user2.DateBirth = new DateTime(1990, 12, 2);
            user2.PhoneNumber = "021 123 45 67";
            user2.DateInscription = new DateTime(2014, 05, 13);
            user2.Password = "1234";

            Profil user2Profil = new Profil();
            user2Profil.Brand = "Mazda";
            user2Profil.Model = "3";
            user2Profil.PlateNumber = "VD 123 45";
            user2Profil.Year = 2017;
            user2Profil.Smoker = false;

            user2.Profils.Add(user2Profil);

            mesUsers.Add(user2);

            return mesUsers;
        }


        public override User getUser(string pAccount)
        {
            User user1 = new User(pAccount);
            user1.Name = "toto";
            user1.FirstName = "tata";
            user1.Address = "Rue d'Yverdon 2";
            user1.City = "Yverdon";
            user1.PostalNumber = 1400;
            user1.DateBirth = new DateTime(1980, 02, 29);
            user1.PhoneNumber = "024 111 11 11";
            user1.DateInscription = new DateTime(2008, 12, 25);
            user1.Password = "1234";

            Profil user1Profil = new Profil();
            user1Profil.Brand = "Toyota";
            user1Profil.Model = "Avenis";
            user1Profil.PlateNumber = "VD 111 11";
            user1Profil.Year = 2015;
            user1Profil.Smoker = true;

            return user1;

        }
    }
}
