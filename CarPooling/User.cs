﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    /// <summary>
    /// 
    /// </summary>
    ///<example>
    ///
    ///</example>
    public class User
    {
        private List<Profil> profils;
        
        private string name;
        private string firstName;
        private DateTime dateBirth;
        private string address;
        private string city;
        private int postalNumber;
        private string phoneNumber;
        private string accountName;
        private string password;
        private DateTime dateInscription;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                firstName = value;
            }
        }

        public DateTime DateBirth
        {
            get
            {
                return dateBirth;
            }

            set
            {
                dateBirth = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }

            set
            {
                address = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }

            set
            {
                city = value;
            }
        }

        public int PostalNumber
        {
            get
            {
                return postalNumber;
            }

            set
            {
                postalNumber = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }

            set
            {
                phoneNumber = value;
            }
        }

        public string AccountName
        {
            get
            {
                return accountName;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public DateTime DateInscription
        {
            get
            {
                return dateInscription;
            }

            set
            {
                dateInscription = value;
            }
        }

        internal List<Profil> Profils
        {
            get
            {
                return profils;
            }
        }

        public User(string pAccount):this()
        {
            accountName = pAccount;
        }

        private User()
        {
            profils = new List<Profil>();
        }
    }
}
