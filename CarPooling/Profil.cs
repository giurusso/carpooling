﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    class Profil
    {
        private string model;
        private string brand;
        private string plateNumber;
        private int year;
        private bool smoker;

        public string Model
        {
            get
            {
                return model;
            }

            set
            {
                model = value;
            }
        }

        public string Brand
        {
            get
            {
                return brand;
            }

            set
            {
                brand = value;
            }
        }

        public string PlateNumber
        {
            get
            {
                return plateNumber;
            }

            set
            {
                plateNumber = value;
            }
        }

        public int Year
        {
            get
            {
                return year;
            }

            set
            {
                year = value;
            }
        }

        public bool Smoker
        {
            get
            {
                return smoker;
            }

            set
            {
                smoker = value;
            }
        }

        public Profil()
        {
        }
    }
}
