﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPooling
{
    public abstract class DataSource:IDataSource
    {

       public abstract List<Trajet> Trajet();
       public abstract List<User> getUsers();
       public abstract User getUser(string pAccount);
     }
}
